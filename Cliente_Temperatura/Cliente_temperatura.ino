/**
   Tecnicas Digitales 3 - UTN FRH
   Sistema de prevencion de Covid-19
   Basgall - Ferraro - De Blasis
*/

/**************************** Includes ****************************/


/* Sensor de temperatura*/


#include <stdio.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
//#include <DHT.h>
#include "DHTesp.h"             // Temperatura 
#include "NewPing.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_MLX90614.h>


/**************************** Defines ****************************/
//Define para el ID y la contraseña de la red
#define STASSID "TD3"
#define STAPSK  "test1234"

//Definimos los pines y el tipo de DHT
#define DHTPIN 16
#define DHTTYPE DHT22

#define ZONA1 0
#define ZONA2 2
#define ZONA3 14
#define ZONA4 15

#define TRIGPIN D6
#define ECHOPIN D7

/**************************** Objetos ****************************/
// Crear el objeto sensor DHT11
DHTesp dht;

// Crear el objeto lcd  dirección  0x3F y 20 columnas x 4 filas
LiquidCrystal_I2C lcd(0x27, 16, 2);

//Crear el objeto la conexion TCP
ESP8266WiFiMulti WiFiMulti;

//Crear el objeto para la conexion UDP
WiFiUDP Udp;

//Crear el objeto del ultrasonido
NewPing sonar(TRIGPIN, ECHOPIN);


//Crear el objeto del termometro
Adafruit_MLX90614 termometroIR = Adafruit_MLX90614();


/**************************** Variables Globales ****************************/
const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "192.168.0.103";
const uint16_t port = 6000;

int Zona = 0;
int Zona1 = 0;
int Zona2 = 0;
int Zona3 = 0;
int Zona4 = 0;

union intermedio {
  char conversor[4];
  float medicion;
} datos;

float humidity = 0;
float temperature = 0;
float distance = 0;
float temperaturaObjeto = 0;

unsigned int localPort = 7000;
char  packetBuffer; // Buffer para los paquetes entrantes
char  rxData[6];     // Buffer para la transmision

/**************************** Funciones ****************************/

void LeerSensores() {

  // Obtener Distancia en cm

  int echoTime = sonar.ping();  // echo time (μs)
  temperature = dht.getTemperature();
  humidity = dht.getHumidity();
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("ERROR DE LECTURA DHT");
    return;
  }
  float vsound = 331.3 + (0.606 * temperature) + (0.0124 * humidity); //Correccion de velocida de sonido
  distance = (echoTime / 2.0) * vsound / 10000; // distance between sensor and target in cm

  // Obtener temperaturas grados Celsius
  temperaturaObjeto = termometroIR.readObjectTempC();

  if ( distance < 10) {
    rxData[5] = 'T';
  } else {
    rxData[5] = 't';
  }
  datos.medicion = temperaturaObjeto;
  rxData[1] = datos.conversor[0];
  rxData[2] = datos.conversor[1];
  rxData[3] = datos.conversor[2];
  rxData[4] = datos.conversor[3];

}

void LCD() {
  if ( distance < 10) {
    //Presento en pantalla temperatura objeto
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(" Termometro: ");
    lcd.setCursor(4, 1);
    lcd.print(temperaturaObjeto);
    lcd.setCursor(9, 1);
    lcd.print("C");
  }  else {
    //Presento en pantalla Temperatura y Humedad
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Temperatura: ");
    lcd.setCursor(12, 0);
    lcd.print(temperature);
    lcd.setCursor(16, 0);
    lcd.print("C");

    lcd.setCursor(0, 1);
    lcd.print("Humedad: ");
    lcd.setCursor(10, 1);
    lcd.print(humidity);
    lcd.setCursor(15, 1);
    lcd.print("%");
  }
}


void DatosSerie() {
}


/**************************** Setup ****************************/
void setup() {

  //pinMode(ZONA1, INPUT);
  //pinMode(ZONA2, INPUT);
  pinMode(ZONA3, INPUT);
  pinMode(ZONA4, INPUT);

  //Zona1 = digitalRead(ZONA1);
 // Zona2 = digitalRead(ZONA2);
  Zona3 = digitalRead(ZONA3);
  Zona4 = digitalRead(ZONA4);

  //int Zona = Zona1 + Zona2 * 2 + Zona3 * 4 + Zona4 * 8;
  Zona = Zona3 + Zona4 * 2;

  // Inicializar el LCD
  lcd.init();

  //Encender la luz de fondo.
  lcd.backlight();
  lcd.clear();
  // Escribimos el Mensaje en el LCD.
  lcd.setCursor(0, 0);
  lcd.print("Proyecto TD3");
  lcd.setCursor(1, 0);
  lcd.clear();
  lcd.print(" Sistema de");
  delay(1000);
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Prevencion");
  lcd.setCursor(1, 1);
  delay(1000);
  lcd.print("para COVID-19");
  delay(1000);


  // Escribimos el Mensaje en el LCD.
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Proyecto TD3");
  lcd.setCursor(1, 1);
  lcd.print("ZONA: ");
  lcd.print(Zona);
  delay(2000);

  // Inicializamos comunicación serie
  lcd.clear();
  lcd.setCursor(1, 0);
  //lcd.print("Comunicacion serie");
  //Serial.begin(115200);
  //lcd.setCursor(9, 1);
  //lcd.print("OK");
  delay(500);

  // Inicializamos la conexion WiFi en modo TCP
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Conectando");
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);
  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi.. ");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  lcd.setCursor(1, 4);
  lcd.print("OK");
  delay(500);
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(500);

  //Inicializar puerto UDP
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("Creando UDP");
   delay(500);
  Serial.printf("UDP server on port %d\n", localPort);
  Udp.begin(localPort);
  lcd.setCursor(2, 4);
  lcd.print("OK");
  delay(500);


  // Inicializamos el sensor DHT
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Sensor DHT");
  Serial.println("Iniciando DHT...");
  dht.setup(DHTPIN, DHTesp::DHT22);
  lcd.setCursor(1, 4);
  lcd.print("OK");
  delay(500);

  // Iniciar termómetro infrarrojo con Arduino
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Temperatura");
  Serial.println("Iniciando sensor de temperatura...");
  termometroIR.begin();
  lcd.setCursor(1, 4);
  lcd.print("OK");
  delay(500);

  //Definimos el dispositivo como un sensor de CO2
  rxData[5] = 't';
}



/**************************** Loop ****************************/
void loop() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Conectando con");
  lcd.setCursor(0, 1);
  lcd.print(host);
  delay(1000);
  Serial.print("Conectando con ");
  Serial.print(host);
  Serial.print(':');
  Serial.println(port);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;

  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    Serial.println("wait 5 sec...");
    delay(5000);
    return;
  } else {
    Serial.println("Conexion TCP establecida escuchamos por UDP la Zona");
    while (client.connected()) {
      int packetSize = Udp.parsePacket();
      if (packetSize) {
        Serial.printf("Received %d packet from %s:%d to %s:%d\n",
                      packetSize,
                      Udp.remoteIP().toString().c_str(), Udp.remotePort(),
                      Udp.destinationIP().toString().c_str(), Udp.localPort());

        // read the packet into packetBufffer
        unsigned int n = Udp.read(&packetBuffer, 1);
        rxData[0] = Zona;
        lcd.clear();
        lcd.setCursor(1, 0);
        lcd.print("TRANSMITIENDO...");
        while (client.connected()) {
          LeerSensores();
          LCD();
          DatosSerie();
          Serial.printf("Transmitimos a %s:%d\n", Udp.remoteIP().toString().c_str(), Udp.remotePort());
          Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
          Udp.write(rxData, sizeof(rxData));
          Udp.endPacket();
          delay(500);
        }
      }
    }
  }
  Serial.println("closing connection");
  client.stop();

  Serial.println("wait 5 sec...");
  delay(5000);
}
