/**
   Tecnicas Digitales 3 - UTN FRH
   Sistema de prevencion de Covid-19
   Basgall - Ferraro - De Blasis
*/

/**************************** Includes ****************************/
#include <stdio.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
#include <DHT.h>
#include <MQ135.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>


/**************************** Defines ****************************/
//Define para el ID y la contraseña de la red
#define STASSID "TD3"
#define STAPSK  "test1234"

//Definimos el pin para el sensor MQ
#define MQPIN A0

//Definimos los pines y el tipo de DHT
#define DHTPIN 0
#define DHTTYPE DHT22


#define ZONA1 14
#define ZONA2 12
#define ZONA3 13
#define ZONA4 16

/**************************** Objetos ****************************/
// Crear el objeto sensor DHT11
DHT dht(DHTPIN, DHTTYPE);

// Crear el objeto sensor MQ135
MQ135 mq135_sensor = MQ135(MQPIN);

// Crear el objeto lcd  dirección  0x3F y 20 columnas x 4 filas
LiquidCrystal_I2C lcd(0x27, 20, 4);

//Crear el objeto la conexion TCP
ESP8266WiFiMulti WiFiMulti;

//Crear el objeto para la conexion UDP
WiFiUDP Udp;


/**************************** Variables Globales ****************************/
const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "192.168.0.103";
const uint16_t port = 6000;

int Zona=0;
int Zona1=0;
int Zona2=0;
int Zona3=0;
bool Zona4=0;

union intermedio {
  char conversor[4];
  float medicion;
} datos;

unsigned int localPort = 7000;
char  packetBuffer; // Buffer para los paquetes entrantes
char  rxData[6];     // Buffer para la transmision
float humidity = 0;
float temperature = 0;
float rzero;
float correctedRZero;
float resistance;
float ppm;
float correctedPPM;
float co2 = 0;


/**************************** Funciones ****************************/

void LeerSensores() {
  humidity = dht.readHumidity();
  temperature = dht.readTemperature();
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  co2 = analogRead(MQPIN);
  rzero = mq135_sensor.getRZero();
  correctedRZero = mq135_sensor.getCorrectedRZero(temperature, humidity);
  resistance = mq135_sensor.getResistance();
  ppm = mq135_sensor.getPPM();
  correctedPPM = 350 + mq135_sensor.getCorrectedPPM(temperature, humidity);
  datos.medicion = correctedPPM;
  rxData[1] = datos.conversor[0];
  rxData[2] = datos.conversor[1];
  rxData[3] = datos.conversor[2];
  rxData[4] = datos.conversor[3];

}

void LCD() {
  //Primera linea
  lcd.setCursor(2, 0);
  lcd.print("Calidad del aire");

  //Segunda linea
  lcd.setCursor(5, 1);
  lcd.print("CO2: ");
  lcd.setCursor(11, 1);
  lcd.print(correctedPPM);
  lcd.setCursor(16, 1);
  lcd.print("PPM");

  //Tercera linea
  lcd.setCursor(4, 2);
  lcd.print("Temp: ");
  lcd.setCursor(11, 2);
  lcd.print(temperature);
  lcd.setCursor(16, 2);
  lcd.print("C");

  //Cuarta linea
  lcd.setCursor(5, 3);
  lcd.print("Hum: ");
  lcd.setCursor(11, 3);
  lcd.print(humidity);
  lcd.setCursor(16, 3);
  lcd.print("%");
}

void DatosSerie() {
  float ppm = mq135_sensor.getPPM();
  Serial.print("Analog: ");
  Serial.println(co2);
  Serial.print("PPMCorregidos: ");
  Serial.println(correctedPPM);
  Serial.print("Temperatura: ");
  Serial.print(temperature);
  Serial.println("ºC");
  Serial.print("Humedad: ");
  Serial.print(humidity);
  Serial.println("%");
  Serial.println("");
  Serial.println("");
}


/**************************** Setup ****************************/
void setup() {

  pinMode(ZONA1, INPUT);
  pinMode(ZONA2, INPUT);
  pinMode(ZONA3, INPUT);
  pinMode(ZONA4, INPUT);

  Zona1=digitalRead(ZONA1);
  Zona2=digitalRead(ZONA2);
  Zona3=digitalRead(ZONA3);
  Zona4=digitalRead(ZONA4);

  Zona=Zona1+Zona2*2+Zona3*4+Zona4*8;
  
  // Inicializar el LCD
  lcd.init();

  //Encender la luz de fondo.
  lcd.backlight();

  // Escribimos el Mensaje en el LCD.
  lcd.setCursor(4, 0);
  lcd.print("Proyecto TD3");
  lcd.setCursor(5, 1);
  lcd.print("Sistema de");
  lcd.setCursor(5, 2);
  lcd.print("prevencion");
  lcd.setCursor(3, 3);
  lcd.print("para  COVID-19");
  delay(1000);  
  
  
  // Escribimos el Mensaje en el LCD.
  lcd.clear();
  lcd.setCursor(4, 0);
  lcd.print("Proyecto TD3");
  lcd.setCursor(6, 1);
  lcd.print("ZONA: ");
  lcd.print(Zona);
  delay(2000);

  // Inicializamos comunicación serie
  lcd.clear();
  lcd.setCursor(1, 1);
  lcd.print("Comunicacion serie");
  Serial.begin(115200);
  lcd.setCursor(9, 2);
  lcd.print("OK");
  delay(500);

  // Inicializamos la conexion WiFi en modo TCP
  lcd.clear();
  lcd.setCursor(1, 1);
  lcd.print("Conectando a la red");
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);
  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  lcd.setCursor(9, 2);
  lcd.print("OK");
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(500);

  //Inicializar puerto UDP
  lcd.clear();
  lcd.setCursor(1, 1);
  lcd.print("Creando puerto UDP");
  Serial.printf("UDP server on port %d\n", localPort);
  Udp.begin(localPort);
  lcd.setCursor(9, 2);
  lcd.print("OK");
  delay(500);

  //Inicializando el Sensor MQ135
  lcd.clear();
  lcd.setCursor(4, 1);
  lcd.print("Sensor MQ135");
  Serial.println("");
  Serial.println("Calentando MQ135..");
  delay(6000);
  lcd.setCursor(9, 2);
  lcd.print("OK");
  Serial.println("LISTO");
  delay(500);

  // Inicializamos el sensor DHT
  lcd.clear();
  lcd.setCursor(5, 1);
  lcd.print("Sensor DHT");
  Serial.println("Iniciando DHT...");
  dht.begin();
  lcd.setCursor(9, 2);
  lcd.print("OK");
  delay(500);

  //Definimos el dispositivo como un sensor de CO2
  rxData[5] = 'G';
}



/**************************** Loop ****************************/
void loop() {
  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print("Conectando con");
  lcd.setCursor(4, 1);
  lcd.print("el servidor");
  lcd.setCursor(9, 2);
  lcd.print("en");
  lcd.setCursor(0, 3);
  lcd.print(host);
  Serial.print("Conectando con ");
  Serial.print(host);
  Serial.print(':');
  Serial.println(port);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;

  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    Serial.println("wait 5 sec...");
    delay(5000);
    return;
  } else {
    Serial.println("Conexion TCP establecida escuchamos por UDP la Zona");
    while (client.connected()) {
      int packetSize = Udp.parsePacket();
      if (packetSize) {
        Serial.printf("Received %d packet from %s:%d to %s:%d\n",
                      packetSize,
                      Udp.remoteIP().toString().c_str(), Udp.remotePort(),
                      Udp.destinationIP().toString().c_str(), Udp.localPort());

        // read the packet into packetBufffer
        unsigned int n = Udp.read(&packetBuffer, 1);
        rxData[0]=Zona;
        lcd.clear();
        lcd.setCursor(2, 0);
        lcd.print("TRANSMITIENDO...");
        while (client.connected()) {
          LeerSensores();
          LCD();
          DatosSerie();
          Serial.printf("Transmitimos a %s:%d\n", Udp.remoteIP().toString().c_str(), Udp.remotePort());
          Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
          Udp.write(rxData, sizeof(rxData));
          Udp.endPacket();
          delay(500);
        }
      }
    }
  }
  Serial.println("closing connection");
  client.stop();

  Serial.println("wait 5 sec...");
  delay(5000);
}
