/**
   Tecnicas Digitales 3 - UTN FRH
   Sistema de prevencion de Covid-19
   Basgall - Ferraro - De Blasis
*/

/***************************************INCLUDES***************************************/


#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <netdb.h>

/***************************************DEFINES***************************************/

#define PORTTCP 6000      			/* Numero de puerto donde se conectaran los sensores */

#define PORTUDP 7000  				/* Numero de puerto donde se reportan los sensores*/
#define TCPGUARDIA 9000  			/* Numero de puerto donde reportamos el estado de ALARMA */
#define MAXCONECTIONS 15      		/* Tamaño de la cola de conexiones recibidas */
#define MAXCHILDS 15      			/* Tamaño de la cola de conexiones recibidas */
#define AlarmaGas 800
#define Alarmatemp 38

#define TIMEOUT 15					/* Tiempo de espera del keepalive*/


/***************************************VARIABLES***************************************/

char ServerIP[]="192.168.0.100"; /* IP donde reportamos el estado de ALARMA */
uint8_t contChilds=0; 				/* Contador de numero de hijos tambien sirve como contador de zona*/
								/*	Las zonas que reciben los sensores se pueden repetir si un hijo muere y se crea otro */


/***************************************PROTOTIPOS***************************************/

void handlerChild(int val);


/***************************************MAIN***************************************/

int main (){
	int socketTCP, socketTCPG;
	int fdtcp, fdtcpg;
	int sin_size = sizeof(struct sockaddr_in);
	int addr_len;
	int numbytes;					/* Para almacenar el numero de bytes enviados/recibidos */
    FILE *fp;
	int fd;

	union intermedio {
		char conversor[4];
		float medicion;
	} datos;
	
	struct sockaddr_in my_addr;		/* Para enviar la zona al sensor */
	struct sockaddr_in their_addr;	/* Para enviar la zona al sensor */
	struct sockaddr_in their_addr2; /* Para reportar el estado de alarma via udp al server 200.69.224.1 */
    
    
    
/*  GUARDIA */
    struct hostent *he;
    if ((socketTCPG = socket(AF_INET, SOCK_STREAM, 0)) == -1){
        perror("Socket");
        exit(1);
    }
    
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(TCPGUARDIA);
    my_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(my_addr.sin_zero), 8);
                           
    /* Bindeamos el socket */
    if ( bind(socketTCPG, (struct sockaddr *)&my_addr,sizeof(struct sockaddr)) == -1){
        perror("BindTCPG");
        exit(1);
    }
    
    /* Preparamos para escuchar hasta maximo 15 conexiones y hijos*/
    printf(" Socket TCP configurado, conectando con guardia\n");
    if (listen(socketTCPG, MAXCONECTIONS) == -1){
        perror("Listen");
        exit(1);
    }
    if ((he=gethostbyname(ServerIP)) == NULL){
        perror("gethostbyname");
        exit(1);
    }
    
    /* Configuramos el puerto=9000 y la IP:200.69.224.1 antes de bindear */
    their_addr.sin_family = AF_INET;
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    bzero(&(their_addr.sin_zero), 8);
    
    
    /* Espero solicitudos y acepto la conexion */
    if ((fdtcpg = accept(socketTCPG, (struct sockaddr *)&their_addr, &sin_size)) == -1){
        perror("Accept");
    }
    printf("Conexion desde:  %s GUARDIA\n",inet_ntoa(their_addr.sin_addr));
    
/*****************************************************************************************************************************************************************************/	
	/* Creamos el socket TCP por donde vamos a escuchar las conexciones entrantes de los sensores */
	if ((socketTCP = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
			perror("Socket");
			exit(1);
	}

	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(PORTTCP);
	my_addr.sin_addr.s_addr = INADDR_ANY;
	bzero(&(my_addr.sin_zero), 8);

	/* Bindeamos el socket */
	if ( bind(socketTCP, (struct sockaddr *)&my_addr,sizeof(struct sockaddr)) == -1){
		perror("BindTCP");
		exit(1);
	}

	/* Preparamos para escuchar hasta maximo 15 conexiones y hijos*/
	printf(" Socket TCP configurado, esperamos un cliente\n");
	if (listen(socketTCP, MAXCONECTIONS) == -1){
		perror("Listen");
		exit(1);
	}
    
        
	while(1){

		/* Espero solicitudos y acepto la conexion */
		if ((fdtcp = accept(socketTCP, (struct sockaddr *)&their_addr, &sin_size)) == -1){
			perror("Accept");
			continue;
		}
        printf("Conexion desde:  %s\n",inet_ntoa(their_addr.sin_addr));
		
		contChilds++;
		/* Creo el hijo que va a administrar el sensor */
		if (!fork()){
			/********************DENTRO DEL HIJO*************************/
			int socketUDP;
            char buf[7];
			/* La data que envien los sensores la guardo aca primer byte es la zona, segundo byte es el estado 1 es alarma, 0 es OK */ 
			char data[6]={0};

			struct hostent *he;
			int selret;
			fd_set rfds;
			struct timeval tv;
			
			/* Creamos el socket UDP */
			if ((socketUDP = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
				perror("Socket");
				exit(1);
			}

			/* Configruo todo para poder bindear el socket UDP */
			their_addr.sin_family = AF_INET;
			their_addr.sin_port = htons(PORTUDP);
			bzero(&(their_addr.sin_zero), 8);

			
			my_addr.sin_port = htons(PORTUDP+contChilds);
		
			/* Bindeo el socket */
			if ( bind(socketUDP, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1){
				perror("BindUDP");
				contChilds--;
				exit(1);
			}
			printf("\n contChilds: %d y Tamaño: %lu\n", contChilds, sizeof(contChilds));

			/* Envio al sensor el numero de zona que le toca, va ser igual al contador de hijos */
			if ((numbytes=sendto(socketUDP,(const void *) &contChilds, sizeof(contChilds), 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))) == -1){
				perror("Sendto");
				exit(1);
			}

			/* Los hijos se van a quedar esperando que los sensores envien la data periodicamente */
			/* Lo implementamos con un select con un timeout, si salimos por timeout cerramos la conexion y matamos al hijo */
			while (1){
				FD_ZERO(&rfds);
				FD_SET(socketUDP, &rfds);
				/* Seteo el TimeOut */
				tv.tv_sec = TIMEOUT;
				tv.tv_usec = 0;

				/* Guardo en selret lo que me devuelvo el select y analizo si es un TimeOut*/
				printf("Entro al select\n");
				selret = select(socketUDP+1, &rfds, NULL, NULL, &tv);			
				if (!selret){
					/* Envio mensaje de TimeOut, cierro el socket y mato al hijo */
					printf("\nSE PERDIÓ CONEXIÓN CON SENSOR\n");
					contChilds--;
					close(socketUDP);
					exit(0);
				} else {
					
					addr_len =sizeof(struct sockaddr);
					/*Si no llego timeout debe haber llegado un mensaje que voy a capturar en data */
					if((numbytes=recvfrom(socketUDP, data, sizeof(data), 0, (struct sockaddr *)&their_addr, &addr_len )) == -1){
						perror("recvfrom");
						exit(0);
					}
					printf("Recibimos %d bytes\n", numbytes);
					datos.conversor[0]=data[1];
					datos.conversor[1]=data[2];
					datos.conversor[2]=data[3];
					datos.conversor[3]=data[4];
                    
                    if(!(fp=fopen("data.txt","a+")))
                        perror("fdopen");
                    
                    
                    fprintf(fp,"%d ",data[0]);
                    fprintf(fp,"%f ",datos.medicion);
                    fprintf(fp,"%c \n",data[5]);
                    fclose(fp);
                    
                    
                    
                    if(data[5]=='G'){
                        
                        printf("\nDato recibido del sensor de gas\n");
                        printf("Zona: %d         Medicion: %f \n", data[0], datos.medicion);

                      	if(datos.medicion > AlarmaGas){
                           	printf("\nALARMA\n");
                           	if(sendto(fdtcpg,data,sizeof(data),0,(struct sockaddr *)&their_addr,sizeof(struct sockaddr))==-1){
						   		perror("send");
                        	}
                        }
                    }
                    
                    if(data[5]=='T'){
                        
                        printf("\nDato recibido del sensor de temperatura corporal\n");
                        printf("Zona: %d         Medicion: %f \n", data[0], datos.medicion);
                        
                      	if(datos.medicion > Alarmatemp){
                           	printf("\nALARMA\n");
                           	if(sendto(fdtcpg,data,sizeof(data),0,(struct sockaddr *)&their_addr,sizeof(struct sockaddr))==-1){
						   		perror("send");
                        	}
                        }
                    }
                    if(data[5]=='t'){
                        
                        printf("\nDato recibido del sensor de temperatura ambiente\n");
                        printf("Zona: %d         Medicion: %f \n", data[0], datos.medicion);
                        
                    }
						
						/* Cierro el socket */
					}
				}
			}
		
		}
	
	close(socketTCP);
    close(fdtcpg);
    close(socketTCPG);
    
}

/* Handler para la muerte del hijo */
void handlerChild(int val){
	/* Decremento el contador de hijos */
	contChilds--;
	wait(0);
}
