/*
 * Union no funciono.
 * me tienen que enviar rxData[6] -> donde tengo en 1 - Zona
 *                                                  2 a 5 - Medicion 
 *                                                  6 - Sensor
 * me conecto y espero a recibir el mesaje por TCP (no pido conexion)
 * 
 * Fijamos IP = 101
*/
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>


#define ANCHO 128
#define ALTO 64

//#define OLED_RESET 4
Adafruit_SSD1306 oled(ANCHO, ALTO, &Wire, -1);

#ifndef STASSID
#define STASSID "TD3"
#define STAPSK  "test1234"
#endif

/******* Variables ************/

union intermedio {
    char conversor[4];
    float medicion;
  } datos;

char ch, buff[6], z[2];
int i=0;

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "192.168.0.103";
const uint16_t port = 9000;
String line;

ESP8266WiFiMulti WiFiMulti;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Wire.begin();
  
  if(!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  
  
  // We start by connecting to a WiFi network
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(ssid, password);

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");

  oled.clearDisplay (); // limpia pantalla      
  oled.drawRect (20, 20, 88, 44, WHITE); // dibuja rectangulo
  oled.setCursor (28, 34); // ubica cursor en coordenadas 28,34
  oled.setTextSize (1); // establece tamano de texto en 2
  oled.setTextColor (WHITE); // establece color al unico disponible (pantalla monocromo)
  oled.println ("wait wifi"); // escribe texto
  oled.display (); // muestra en pantalla todo lo establecido anteriormente

  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);
Serial.println(WiFi.localIP()); //Obtenemos la IP
}

void loop() {

  oled.clearDisplay (); // limpia pantalla      
  oled.setCursor (28, 34); // ubica cursor en coordenadas 28,34
  oled.setTextSize (1); // establece tamano de texto en 2
  oled.setTextColor (WHITE); // establece color al unico disponible (pantalla monocromo)
  oled.print ("Conectando"); // escribe texto
  oled.display (); // muestra en pantalla todo lo establecido anteriormente
 
  Serial.print("connecting to ");
  Serial.print(host);
  Serial.print(':');
  Serial.println(port);

  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
      oled.clearDisplay (); // limpia pantalla      
      oled.setCursor (20, 34); // ubica cursor en coordenadas 28,34
      oled.setTextSize (2); // establece tamano de texto en 2
      oled.setTextColor (WHITE); // establece color al unico disponible (pantalla monocromo)
      oled.print ("Esperando"); // escribe texto
      oled.display (); // muestra en pantalla todo lo establecido anteriormente
    delay(5000);
    return;
  }

  
  while(client.connected()){
    Serial.println("conectado con servidor");
    if(client.available()){
    while(i<6){
      ch = static_cast<char>(client.read());
     buff[i]=ch;
      i++;
      
    }
    i=0;
    Serial.print(buff);
    datos.conversor[0]=buff[1];
    datos.conversor[1]=buff[2];
    datos.conversor[2]=buff[3];
    datos.conversor[3]=buff[4];

    itoa(buff[0],z,10);
    
    if(buff[5]=='G'||buff[5]=='T'||buff[5]=='t'){
      /**** ALARMA *******/
    oled.clearDisplay (); // limpia pantalla      
    oled.drawRect (20, 20, 88, 44, WHITE); // dibuja rectangulo
    oled.setCursor (28, 34); // ubica cursor en coordenadas 28,34
    oled.setTextSize (2); // establece tamano de texto en 2
    oled.setTextColor (WHITE); // establece color al unico disponible (pantalla monocromo)
    oled.print ("ALARMA"); // escribe texto
    oled.display (); // muestra en pantalla todo lo establecido anteriormente
    delay (250);

    /**** ZONA *******/
    oled.clearDisplay (); // limpia pantalla
    oled.setTextColor (WHITE); // establece color al unico disponible (pantalla monocromo)
    oled.setCursor (0, 0); // ubicar cursor en inicio de coordenadas 0,0
    oled.setTextSize (2); // establece tamano de texto en 1
    oled.print ("Zona: "); // escribe en pantalla el texto
    oled.print(z);
    oled.display (); // muestra en pantalla todo lo establecido anteriormente
    delay (500);

    /**** MEDICION *******/
    oled.clearDisplay (); // limpia pantalla
    oled.setCursor (0, 0); // ubica cursor en coordenas 10,30
    oled.setTextSize (2); // establece tamano de texto en 2
    //oled.print (milis () / 1000); // escribe valor de millis () dividido por 1000
    oled.print ("Medicion: "); // escribe texto
    oled.print (datos.medicion);
    oled.display (); // muestra en pantalla todo lo establecido anteriormente
    delay (500);

    /**** SENSOR *******/
    oled.clearDisplay (); // limpia pantalla
    oled.setCursor (0, 0); // ubica cursor en coordenas 10,30
    oled.setTextSize (2); // establece tamano de texto en 2
    //oled.print (milis () / 1000); // escribe valor de millis () dividido por 1000
    oled.print ("Sensor: "); // escribe texto
    oled.print (buff[5]);
    oled.display (); // muestra en pantalla todo lo establecido anteriormente
    delay (500);
    }
   }
   
     oled.clearDisplay (); // limpia pantalla      
      oled.setCursor (20, 34); // ubica cursor en coordenadas 28,34
      oled.setTextSize (2); // establece tamano de texto en 2
      oled.setTextColor (WHITE); // establece color al unico disponible (pantalla monocromo)
      oled.print ("Esperando servidor"); // escribe texto
      oled.display (); // muestra en pantalla todo lo establecido anteriormente
  }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
